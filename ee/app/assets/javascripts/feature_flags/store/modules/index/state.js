export default () => ({
  featureFlags: [],
  count: {},
  pageInfo: {},
  isLoading: true,
  hasError: false,
  endpoint: null,
  options: {},
});
